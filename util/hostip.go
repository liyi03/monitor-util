package util

import (
	"net"
)

// GetLocalIP 获取本机的ip地址（ipv4）
func GetLocalIP() (ip string, err error) {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return "", err // 立即返回错误
	}

	for _, addr := range addrs {
		ipAddr, ok := addr.(*net.IPNet)
		if !ok {
			continue
		}
		if ipAddr.IP.IsLoopback() || !ipAddr.IP.IsGlobalUnicast() || ipAddr.IP.To4() == nil {
			continue
		}
		ip = ipAddr.IP.String()
		return ip, nil
	}

	return "", nil // 如果没有找到IPv4地址，返回空字符串和nil错误
}
